
console.log('PhantomJS started TestRunner which loads a web page with a TeamCity Reporter...');

var page = require('webpage').create(),
system = require('system'), address;

//Open local teamcity_reporter.html
if (system.args.length === 1) {
    console.log('Usage: phantom.js <some URL>');
    phantom.exit();
}

address = system.args[1];

phantom.viewportSize = {
	width : 800,
	height : 600
};

// Required because PhantomJS sandboxes the website and doesn't show up the
// console messages form that page by default
page.onConsoleMessage = function(msg) {
	console.log(msg); // Pass all page logs to stdout

	if (msg && msg.indexOf("##jasmine.reportRunnerResults") !== -1) {
		phantom.exit();
	}
};

// Open the website with the teamcity reporter
page.open(address, function(status) {
	// Page is loaded!
	if (status !== 'success') {
		console.log('Unable to load the address!');
	} else {
		// Using a delay to make sure the JavaScript is executed in the browser
		window.setTimeout(function() {
			page.render("output.png");
			phantom.exit();
		}, 1000);
	}
});