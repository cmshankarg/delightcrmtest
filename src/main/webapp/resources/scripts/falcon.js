'use strict';

angular.module('falcon', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ngRoute'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'main/layout',
        controller: 'MainController'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
;
