module.exports = function(config){

  config.set({
    basePath : '..', //!\\ Ignored through gulp-karma //!\\

    files : [ //!\\ Ignored through gulp-karma //!\\
        'resources/bower_components/angular/angular.js',
        'resources/bower_components/angular/angular-route.js',
        'resources/bower_components/angular-mocks/angular-mocks.js',
        'resources/scripts/** /*.js',
        'test/unit/** /*.js'
    ],

    autoWatch : false,

    frameworks: ['jasmine'],

    browsers : ['PhantomJS'],

    plugins : [
        'karma-phantomjs-launcher',
        'karma-jasmine'
    ]

})}
