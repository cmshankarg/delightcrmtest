'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();

// inject bower components
gulp.task('wiredep', function () {
  var wiredep = require('wiredep').stream;

  gulp.src('resources/styles/*.scss')
    .pipe(wiredep({
        directory: 'resources/bower_components'
    }))
    .pipe(gulp.dest('resources/styles'));

  gulp.src('resources/*.html')
    .pipe(wiredep({
      directory: 'resources/bower_components',
      exclude: ['bootstrap-sass-official']
    }))
    .pipe(gulp.dest('resources'));
});
