'use strict';

var gulp = require('gulp');

var browserSync = require('browser-sync');

var $ = require('gulp-load-plugins')();

gulp.task('watch', ['wiredep', 'styles'] ,function () {
  gulp.watch('resources/styles/**/*.scss', ['styles']);
  gulp.watch('resources/scripts/**/*.js', ['scripts']);
  gulp.watch('resources/images/**/*', ['images']);
  gulp.watch('bower.json', ['wiredep']);
});
